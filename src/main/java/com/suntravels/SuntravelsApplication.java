package com.suntravels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.test", "com.bean"})
@ComponentScan({"com.suntravels","com.bean"})
@EntityScan("com.suntravels")
@EnableJpaRepositories("com.suntravels.repository")
public class SuntravelsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuntravelsApplication.class, args);
	}

}
