package com.suntravels.repository;

import com.suntravels.model.Hotel;
import com.suntravels.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room,Integer>
{
    @Query(value = "SELECT h.room_types FROM Hotel h WHERE h.hotel_id=?1 ")
    List<Room> findRoomByHotel( int hotel_id);
}
