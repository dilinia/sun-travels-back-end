package com.suntravels.repository;

import com.suntravels.model.Contract;
import com.suntravels.model.ContractRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRoomRepository extends JpaRepository<ContractRoom,Integer>
{
    @Query(value = "SELECT cr FROM ContractRoom cr WHERE contract_id2=?1 ")
    List<ContractRoom> findContractRoomById( int contract_id);
}
