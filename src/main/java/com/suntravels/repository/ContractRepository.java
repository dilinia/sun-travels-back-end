package com.suntravels.repository;

import com.suntravels.model.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends JpaRepository<Contract,Integer>
{
    @Query(value = "SELECT c FROM Contract c WHERE c.hotel.hotel_id=?1 ")
    List<Contract> findContractById( int hotel_id);
}
