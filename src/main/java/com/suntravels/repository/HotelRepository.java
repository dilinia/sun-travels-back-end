package com.suntravels.repository;

import com.suntravels.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends JpaRepository<Hotel,Integer>
{
    @Query(value = "SELECT h FROM Hotel h WHERE h.branch=?1 AND h.name=?2 ")
    List<Hotel> findHotelByBranch(String branch, String name);

    @Query(value = "SELECT h FROM Hotel h WHERE h.branch=?1 ")
    List<Hotel> findHotelByLocation(String branch);

}
