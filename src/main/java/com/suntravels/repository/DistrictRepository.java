package com.suntravels.repository;

import com.suntravels.model.District;
import com.suntravels.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository<District,Integer>
{
}
