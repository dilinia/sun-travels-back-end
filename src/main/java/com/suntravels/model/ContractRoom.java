package com.suntravels.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name= "contract_roomtype")
@Getter
@Setter
public class ContractRoom
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "contract_roomtype_id")
    private int contract_roomtype_id;

    @Column(name = "price")
    private float price;

    @Column(name = "no_of_rooms")
    private int no_of_rooms;

    @Column(name = "max_adults")
    private int max_adults;

    @Column(name = "contract_roomtypes_contract_id")
    private int contract_id2;

    @OneToOne(targetEntity = Room.class)
    @JoinColumn( referencedColumnName = "roomtype_id")
    private Room room;

}
