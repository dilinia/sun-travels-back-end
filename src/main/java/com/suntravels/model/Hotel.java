package com.suntravels.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

@Entity
@Table(name= "hotel_details")
@Getter
@Setter
public class Hotel
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "hotel_id")
    private int hotel_id;

    @Column(name = "name")
    private String name;

    @Column(name = "branch")
    private String branch;

    @Column(name = "email")
    private String email;

    @Column(name = "telephone_no")
    private String telephone_no;

    @OneToMany(targetEntity = Room.class, cascade=CascadeType.ALL)
    @JoinColumn(referencedColumnName = "hotel_id")
    private List<Room> room_types;

}
