package com.suntravels.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "room_type")
@Getter
@Setter
public class Room
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "roomtype_id")
    private int roomtype_id;

    @Column(name = "type_name")
    private String type_name;

    @Column(name = "description")
    private String description;


}
