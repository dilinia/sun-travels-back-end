package com.suntravels.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Entity
@Table(name= "paper_contracts")
@Getter
@Setter
public class Contract
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "contract_id")
    private int contract_id;

    @Column(name = "start_date")
    private Date start_date;

    @Column(name = "end_date")
    private Date end_date;

    @Column(name = "markup")
    private int markup;

    @ManyToOne(targetEntity = Hotel.class)
    @JoinColumn(referencedColumnName = "hotel_id")
    private Hotel hotel;

    @OneToMany(targetEntity = ContractRoom.class, cascade=CascadeType.ALL)
    @JoinColumn( referencedColumnName = "contract_id")
    private List<ContractRoom> contract_roomtypes;


}
