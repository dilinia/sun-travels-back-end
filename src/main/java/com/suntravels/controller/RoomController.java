package com.suntravels.controller;

import com.suntravels.model.Hotel;
import com.suntravels.model.Room;
import com.suntravels.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class RoomController
{
    @Autowired
    RoomService roomService;

    @GetMapping("/rooms")
    public ResponseEntity<List<Room>> get(){
        List<Room> rooms = roomService.findAll();
        return new ResponseEntity<List<Room>>(rooms, HttpStatus.OK);
    }

    @PostMapping("/rooms")
    public ResponseEntity<Room> save( @RequestBody Room room){
        Room room_ = roomService.save(room);
        return new ResponseEntity<Room>(room_, HttpStatus.OK);
    }

    @GetMapping("/roomByHotel")
    public ResponseEntity<List<Room>> findRoomByHotel( @RequestParam int hotel_id){
        List<Room> roomByHotel = roomService.findRoomByHotel(hotel_id);
        return new ResponseEntity<List<Room>>(roomByHotel, HttpStatus.OK);
    }
}
