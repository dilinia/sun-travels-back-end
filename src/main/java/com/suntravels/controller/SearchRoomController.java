package com.suntravels.controller;

import com.suntravels.model.Hotel;
import com.suntravels.service.HotelService;
import com.suntravels.service.SearchRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class SearchRoomController
{
    @Autowired
    SearchRoomService searchRoomService;

    @GetMapping("/availability")
    public ResponseEntity<String> availabilityOfContract( @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date checkinDate, @RequestParam int noOfNights, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date endDate){
        if(searchRoomService.checkStartDate(checkinDate,startDate,endDate) & searchRoomService.checkNoOfNights( noOfNights, startDate, endDate )){
            return new ResponseEntity<String>("Available",HttpStatus.OK );
        }else{
            return new ResponseEntity<String>("Not Available",HttpStatus.OK );
        }
    }
}
