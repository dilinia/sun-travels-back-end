package com.suntravels.controller;

import com.suntravels.model.District;
import com.suntravels.model.Hotel;
import com.suntravels.service.DistrictService;
import com.suntravels.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class HotelController
{
    @Autowired
    HotelService hotelService;

    @Autowired
    DistrictService districtService;

    @GetMapping("/gethotels")
    public ResponseEntity<List<Hotel>> get(){
        List<Hotel> hotels = hotelService.findAll();
        return new ResponseEntity<List<Hotel>>(hotels, HttpStatus.OK);
    }

    @GetMapping("/districts")
    public ResponseEntity<List<District>> getDistricts(){
        List<District> districts = districtService.findAll();
        return new ResponseEntity<List<District>>(districts, HttpStatus.OK);
    }

    @PostMapping("/hotels")
    public ResponseEntity<Hotel> save( @RequestBody Hotel hotel){
        Hotel hotel_ = hotelService.save(hotel);
        return new ResponseEntity<Hotel>(hotel_, HttpStatus.OK);
    }

    @GetMapping("/hotelByBranch")
    public ResponseEntity<List<Hotel>> findHotelById( @RequestParam String branch, @RequestParam String name){
    List<Hotel> hotelByBranch = hotelService.findHotelByBranch( branch, name );
    return new ResponseEntity<>(hotelByBranch, HttpStatus.OK);
    }

    @GetMapping("/hotelByLocation")
    public ResponseEntity<List<Hotel>> findHotelById( @RequestParam String branch){
        List<Hotel> hotelByLocation = hotelService.findHotelByLocation(branch);
        return new ResponseEntity<>(hotelByLocation, HttpStatus.OK);
    }
}
