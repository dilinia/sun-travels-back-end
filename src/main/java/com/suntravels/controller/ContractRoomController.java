package com.suntravels.controller;

import com.suntravels.model.Contract;
import com.suntravels.model.ContractRoom;
import com.suntravels.service.ContractRoomService;
import com.suntravels.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class ContractRoomController
{
    @Autowired
    ContractRoomService contractRoomService;

    @GetMapping( "/contractRoomById" )
    public ResponseEntity<List<ContractRoom>> findContractRoomById( @RequestParam int contract_id )
    {
        List<ContractRoom> contractRoomById = contractRoomService.findContractRoomById( contract_id );
        return new ResponseEntity<>( contractRoomById, HttpStatus.OK );
    }
}
