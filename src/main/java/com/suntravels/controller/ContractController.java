package com.suntravels.controller;

import com.suntravels.model.Contract;
import com.suntravels.model.Hotel;
import com.suntravels.service.ContractService;
import com.suntravels.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class ContractController
{
    @Autowired
    ContractService contractService;

    @PostMapping( "/contracts" )
    public ResponseEntity<Contract> save( @RequestBody Contract contract )
    {
        Contract contract_ = contractService.save( contract );
        return new ResponseEntity<Contract>( contract_, HttpStatus.OK );
    }

    @GetMapping( "/contractById" )
    public ResponseEntity<List<Contract>> findContractById( @RequestParam int hotel_id )
    {
        List<Contract> contractById = contractService.findContractById( hotel_id );
        return new ResponseEntity<>( contractById, HttpStatus.OK );
    }
}
