package com.suntravels.service;

import com.suntravels.model.District;
import com.suntravels.model.Hotel;

import java.util.List;

public interface DistrictService
{
    List<District> findAll();
}
