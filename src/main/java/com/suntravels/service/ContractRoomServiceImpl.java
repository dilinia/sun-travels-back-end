package com.suntravels.service;

import com.suntravels.model.Contract;
import com.suntravels.model.ContractRoom;
import com.suntravels.repository.ContractRepository;
import com.suntravels.repository.ContractRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractRoomServiceImpl implements ContractRoomService
{
    @Autowired
    ContractRoomRepository contractRoomRepository;

    @Override
    public List<ContractRoom> findContractRoomById( int contract_id){
        return contractRoomRepository.findContractRoomById(contract_id);
    }
}
