package com.suntravels.service;

import com.suntravels.model.District;
import com.suntravels.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistrictServiceImple implements DistrictService
{
    @Autowired
    DistrictRepository districtRepository;

    @Override
    public List<District> findAll(){
        return districtRepository.findAll();
    };
}
