package com.suntravels.service;

import com.suntravels.model.Hotel;
import com.suntravels.model.Room;
import com.suntravels.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService
{
    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<Room> findAll(){
        return roomRepository.findAll();
    };

    @Override
    public Room save(Room room){
        roomRepository.save( room );
        return room;
    }

    @Override
    public List<Room> findRoomByHotel( int hotel_id){
        return roomRepository.findRoomByHotel(hotel_id);
    }

}
