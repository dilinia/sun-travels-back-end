package com.suntravels.service;

import com.suntravels.model.Contract;
import com.suntravels.model.Hotel;
import com.suntravels.model.Room;

import java.util.List;

public interface ContractService
{

    Contract save( Contract contract);

    List<Contract> findContractById(int hotel_id);

}
