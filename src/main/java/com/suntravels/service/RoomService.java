package com.suntravels.service;

import com.suntravels.model.Hotel;
import com.suntravels.model.Room;

import java.util.List;

public interface RoomService
{
    List<Room> findAll();

    Room save(Room room);

    List<Room> findRoomByHotel( int hotel_id);

}
