package com.suntravels.service;

import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SearchRoomServiceImpl implements SearchRoomService
{
    public boolean checkStartDate( Date checkinDate, Date startDate, Date endDate){
        if( checkinDate.after( startDate ) & checkinDate.before( endDate )){
            return true;
        }else{
            return false;
        }
    }

    public boolean checkNoOfNights( int NoOfNights , Date startDate, Date endDate){
        int daysdiff = 0;
        long diff = endDate.getTime() - startDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
        daysdiff = (int) diffDays;

        if(NoOfNights <= daysdiff){
            return true;
        }else{
            return false;
        }
    }



}
