package com.suntravels.service;

import com.suntravels.model.Contract;
import com.suntravels.model.Hotel;
import com.suntravels.repository.ContractRepository;
import com.suntravels.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractServiceImpl implements ContractService
{
    @Autowired
    ContractRepository contractRepository;

    @Override
    public Contract save( Contract contract){
        contractRepository.save( contract );
        return contract;
    }

    @Override
    public List<Contract> findContractById(int hotel_id){
        return contractRepository.findContractById(hotel_id);
    }
}
