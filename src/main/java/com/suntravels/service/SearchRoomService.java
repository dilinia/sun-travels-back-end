package com.suntravels.service;

import java.util.Date;

public interface SearchRoomService
{
    boolean checkStartDate( Date checkinDate, Date startDate, Date endDate);

    boolean checkNoOfNights( int NoOfNights , Date startDate, Date endDate);
}
