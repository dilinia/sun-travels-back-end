package com.suntravels.service;

import com.suntravels.model.Contract;
import com.suntravels.model.ContractRoom;
import com.suntravels.model.Hotel;
import com.suntravels.model.Room;

import java.util.List;

public interface ContractRoomService
{
    List<ContractRoom> findContractRoomById( int contract_id);

}
