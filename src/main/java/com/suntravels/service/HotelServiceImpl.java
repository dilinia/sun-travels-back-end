package com.suntravels.service;

import com.suntravels.model.District;
import com.suntravels.model.Hotel;
import com.suntravels.repository.DistrictRepository;
import com.suntravels.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelServiceImpl implements HotelService
{
    @Autowired
    HotelRepository hotelRepository;

    @Override
    public List<Hotel> findAll(){
        return hotelRepository.findAll();
    };


    @Override
    public Hotel save(Hotel hotel){
        hotelRepository.save( hotel );
        return hotel;
    }

    @Override
    public List<Hotel> findHotelByBranch(String branch, String name){
        return hotelRepository.findHotelByBranch(branch, name);
    }

    @Override
    public List<Hotel> findHotelByLocation(String branch){
        return hotelRepository.findHotelByLocation(branch);
    }

}
